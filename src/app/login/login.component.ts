import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string ="";
  password: string ="";
  constructor(private router: Router,private http: HttpClient) {}
 
  Login():void {
    this.router.navigate(['/profile']);
    console.log(this.email);
    console.log(this.password);
 
    let bodyData = {
      email: this.email,
      password: this.password,
    };  

    const headers = new HttpHeaders(bodyData ? { authorization: 'Basic ' + btoa(bodyData.email + ':' + bodyData.password) } : {});
        this.http.post("http://localhost:8080/login", headers).subscribe(  (resultData: any) => {
        console.log(resultData);
 
        if (resultData.message == "Email not exits")
        {
      
          alert("Email not exits");
    
 
        }
        else if(resultData.message == "Login Success")
        {
          // this.router.navigateByUrl('/home');
          alert("Login Success");

        }
        else
        {
          alert("Incorrect Email and Password not match");
        }
      });
    }
    
  
}
